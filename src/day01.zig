const std = @import("std");
const testing = std.testing;
const Allocator = std.mem.Allocator;
const List = std.ArrayList;
const Map = std.AutoHashMap;
const StrMap = std.StringHashMap;
const BitSet = std.DynamicBitSet;

const util = @import("util.zig");
const gpa = util.gpa;

const data = @embedFile("data/day01.txt");

pub fn main() !void {
    var input: []const u8 = data;

    var max_cals = try maxCalories(input);
    print("most Calories: {d}\n", .{max_cals});

    var top_three = try topThreeCalories(input);
    print("top three Calories: {d}\n", .{top_three});
}

pub fn maxCalories(input: []const u8) !usize {
    var outer = split(u8, input, "\n\n");
    var calories = List(usize).init(gpa);

    while (outer.next()) |tok| {
        var inner = tokenize(u8, tok, "\n");
        var total_cals: usize = 0;
        while (inner.next()) |c| {
            total_cals += try parseInt(usize, c, 0);
        }
        try calories.append(total_cals);
    }

    return sliceMax(usize, calories.items);
}

pub fn topThreeCalories(input: []const u8) !usize {
    var outer = split(u8, input, "\n\n");
    var calories = List(usize).init(gpa);

    while (outer.next()) |tok| {
        var inner = tokenize(u8, tok, "\n");
        var total_cals: usize = 0;
        while (inner.next()) |c| {
            total_cals += try parseInt(usize, c, 0);
        }
        try calories.append(total_cals);
    }

    sort(usize, calories.items, {}, desc(usize));

    var top_three = calories.items[0] +
        calories.items[1] + calories.items[2];
    return top_three;
}

test "max calories" {
    var input = (
        \\1000
        \\2000
        \\3000
        \\
        \\4000
        \\
        \\5000
        \\6000
        \\
        \\7000
        \\8000
        \\9000
        \\
        \\10000
    );

    var max_cals = try maxCalories(input);
    try testing.expectEqual(max_cals, 24000);
}

test "max calories" {
    var input = (
        \\1000
        \\2000
        \\3000
        \\
        \\4000
        \\
        \\5000
        \\6000
        \\
        \\7000
        \\8000
        \\9000
        \\
        \\10000
    );

    var top_three = try topThreeCalories(input);
    try testing.expectEqual(top_three, 45000);
}

// Useful stdlib functions
const tokenize = std.mem.tokenize;
const split = std.mem.split;
const indexOf = std.mem.indexOfScalar;
const indexOfAny = std.mem.indexOfAny;
const indexOfStr = std.mem.indexOfPosLinear;
const lastIndexOf = std.mem.lastIndexOfScalar;
const lastIndexOfAny = std.mem.lastIndexOfAny;
const lastIndexOfStr = std.mem.lastIndexOfLinear;
const trim = std.mem.trim;
const sliceMin = std.mem.min;
const sliceMax = std.mem.max;

const parseInt = std.fmt.parseInt;
const parseFloat = std.fmt.parseFloat;

const min = std.math.min;
const min3 = std.math.min3;
const max = std.math.max;
const max3 = std.math.max3;

const print = std.debug.print;
const assert = std.debug.assert;

const sort = std.sort.sort;
const asc = std.sort.asc;
const desc = std.sort.desc;

// Generated from template/template.zig.
// Run `zig build generate` to update.
// Only unmodified days will be updated.
