const std = @import("std");
const testing = std.testing;
const Allocator = std.mem.Allocator;
const List = std.ArrayList;
const Map = std.AutoHashMap;
const StrMap = std.StringHashMap;
const BitSet = std.DynamicBitSet;

const util = @import("util.zig");
const gpa = util.gpa;

const data = @embedFile("data/day02.txt");

const Shape = enum(u8) {
    rock = 1,
    paper = 2,
    scissors = 3,
};

const Result = enum(u8) {
    lose = 0,
    draw = 3,
    win = 6,
};

pub fn main() !void {
    var input: []const u8 = data;
    var score = try finalScore(input);
    print("final score: {d}\n", .{score});

    score = try finalScoreByResult(input);
    print("final score: {d}\n", .{score});
}

pub fn finalScore(input: []const u8) !usize {
    var outer = tokenize(u8, input, "\n");
    var total_score: usize = 0;

    while (outer.next()) |r| {
        var inner = tokenize(u8, r, " ");

        var op_shape = try strToShape(inner.next().?[0]);
        var me_shape = try strToShape(inner.next().?[0]);

        var score: usize = round(op_shape, me_shape);

        total_score += score;
    }

    return total_score;
}

pub fn finalScoreByResult(input: []const u8) !usize {
    var outer = tokenize(u8, input, "\n");
    var total_score: usize = 0;

    while (outer.next()) |r| {
        var inner = tokenize(u8, r, " ");

        var op_shape = try strToShape(inner.next().?[0]);
        var me_result = try strToResult(inner.next().?[0]);

        var score: usize = roundByResult(op_shape, me_result);

        total_score += score;
    }

    return total_score;
}

pub fn round(op: Shape, me: Shape) u8 {
    var score: u8 = @enumToInt(me);
    if (op == me) {
        score += @enumToInt(Result.draw);
        return score;
    }

    switch (me) {
        .rock => {
            switch (op) {
                .paper => {
                    score += @enumToInt(Result.lose);
                },
                .scissors => {
                    score += @enumToInt(Result.win);
                },
                else => {},
            }
        },
        .paper => {
            switch (op) {
                .rock => {
                    score += @enumToInt(Result.win);
                },
                .scissors => {
                    score += @enumToInt(Result.lose);
                },
                else => {},
            }
        },
        .scissors => {
            switch (op) {
                .rock => {
                    score += @enumToInt(Result.lose);
                },
                .paper => {
                    score += @enumToInt(Result.win);
                },
                else => {},
            }
        },
    }
    return score;
}

pub fn strToShape(str: u8) !Shape {
    switch (str) {
        'A', 'X' => {
            return Shape.rock;
        },
        'B', 'Y' => {
            return Shape.paper;
        },
        'C', 'Z' => {
            return Shape.scissors;
        },
        else => {
            return error.UnexpectedError;
        },
    }
}

pub fn roundByResult(op: Shape, me: Result) u8 {
    var score: u8 = @enumToInt(me);
    if (me == Result.draw) {
        score += @enumToInt(op);
        return score;
    }

    switch (me) {
        .lose => {
            switch (op) {
                .paper => {
                    score += @enumToInt(Shape.rock);
                },
                .scissors => {
                    score += @enumToInt(Shape.paper);
                },
                .rock => {
                    score += @enumToInt(Shape.scissors);
                },
            }
        },
        .win => {
            switch (op) {
                .rock => {
                    score += @enumToInt(Shape.paper);
                },
                .scissors => {
                    score += @enumToInt(Shape.rock);
                },
                .paper => {
                    score += @enumToInt(Shape.scissors);
                },
            }
        },
        else => {},
    }
    return score;
}

pub fn strToResult(str: u8) !Result {
    switch (str) {
        'X' => {
            return Result.lose;
        },
        'Y' => {
            return Result.draw;
        },
        'Z' => {
            return Result.win;
        },
        else => {
            return error.UnexpectedError;
        },
    }
}

test "final score" {
    var input = (
        \\A Y
        \\B X
        \\C Z
    );

    var score = try finalScore(input);
    try testing.expectEqual(score, 15);
}

test "final score by result" {
    var input = (
        \\A Y
        \\B X
        \\C Z
    );

    var score = try finalScoreByResult(input);
    try testing.expectEqual(score, 12);
}

// Useful stdlib functions
const tokenize = std.mem.tokenize;
const split = std.mem.split;
const indexOf = std.mem.indexOfScalar;
const indexOfAny = std.mem.indexOfAny;
const indexOfStr = std.mem.indexOfPosLinear;
const lastIndexOf = std.mem.lastIndexOfScalar;
const lastIndexOfAny = std.mem.lastIndexOfAny;
const lastIndexOfStr = std.mem.lastIndexOfLinear;
const trim = std.mem.trim;
const sliceMin = std.mem.min;
const sliceMax = std.mem.max;

const parseInt = std.fmt.parseInt;
const parseFloat = std.fmt.parseFloat;

const min = std.math.min;
const min3 = std.math.min3;
const max = std.math.max;
const max3 = std.math.max3;

const print = std.debug.print;
const assert = std.debug.assert;

const sort = std.sort.sort;
const asc = std.sort.asc;
const desc = std.sort.desc;

// Generated from template/template.zig.
// Run `zig build generate` to update.
// Only unmodified days will be updated.
