const std = @import("std");
const testing = std.testing;
const Allocator = std.mem.Allocator;
const List = std.ArrayList;
const Map = std.AutoHashMap;
const StrMap = std.StringHashMap;
const BitSet = std.DynamicBitSet;

const util = @import("util.zig");
const gpa = util.gpa;

const data = @embedFile("data/day03.txt");

pub fn main() !void {
    var input: []const u8 = data;
    var priority_map = try PriorityMap.init();
    var sum = try piroritySum(input, priority_map);
    print("part1: {d}\n", .{sum});

    sum = try groupBadgeSum(input, priority_map);
    print("part2: {d}\n", .{sum});
}

pub fn piroritySum(input: []const u8, m: PriorityMap) !usize {
    var it = tokenize(u8, input, "\n");
    var sum: usize = 0;

    while (it.next()) |r| {
        var common = try findCommonItem(r);
        if (m.map.get(common)) |item| {
            sum += item;
        }
    }
    return sum;
}

pub fn groupBadgeSum(input: []const u8, m: PriorityMap) !usize {
    var it = tokenize(u8, input, "\n");
    var sum: usize = 0;
    var index: usize = 0;
    var group: [3][]const u8 = undefined;

    while (it.next()) |r| {
        group[index] = r;

        if (index == 2) {
            var common = try findGroupBadge(group);
            if (m.map.get(common)) |item| {
                sum += item;
            }
            index = 0;
        } else {
            index += 1;
        }
    }
    return sum;
}

pub fn findCommonItem(ruckstack: []const u8) !u8 {
    var half = ruckstack.len / 2;
    var first_half = ruckstack[0..half];
    var second_half = ruckstack[half..];

    var common_item: u8 = undefined;
    for (first_half) |item| {
        if (lastIndexOf(u8, second_half, item)) |idx| {
            common_item = second_half[idx];
        }
    }

    if (common_item == undefined) {
        return error.UnexpectedError;
    }

    return common_item;
}

pub fn findGroupBadge(group: [3][]const u8) !u8 {
    var common_items = List(u8).init(gpa);
    var common_item: u8 = undefined;

    for (group[0]) |item| {
        if (lastIndexOf(u8, group[1], item)) |idx| {
            try common_items.append(group[1][idx]);
        }
    }

    for (common_items.items) |item| {
        if (lastIndexOf(u8, group[2], item)) |idx| {
            common_item = group[2][idx];
        }
    }

    if (common_item == undefined) {
        return error.UnexpectedError;
    }

    return common_item;
}

const PriorityMap = struct {
    map: Map(u8, u8),

    const Self = @This();
    pub fn init() !Self {
        var m = Map(u8, u8).init(gpa);

        var i: u8 = 'A';
        while (i <= 'Z') : (i += 1) {
            try m.put(i, i - 'A' + 27);
        }

        var j: u8 = 'a';
        while (j <= 'z') : (j += 1) {
            try m.put(j, j - 'a' + 1);
        }
        return Self{ .map = m };
    }
};

test "priority sum" {
    var input = (
        \\VJrwpWtwJgWrhcsFMMfFFhFp
        \\JqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
        \\PmmdzqPrVvPwwTWBwg
        \\WMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
        \\TtgJtRGJQctTZtZT
        \\CrZsJsPPZsGzwwsLwLmpwMDw
    );

    var priority_map = try PriorityMap.init();

    var sum = try piroritySum(input, priority_map);
    try testing.expectEqual(sum, 157);
}

test "badge sum" {
    var input = (
        \\VJrwpWtwJgWrhcsFMMfFFhFp
        \\JqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
        \\PmmdzqPrVvPwwTWBwg
        \\WMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
        \\TtgJtRGJQctTZtZT
        \\CrZsJsPPZsGzwwsLwLmpwMDw
    );

    var priority_map = try PriorityMap.init();

    var sum = try groupBadgeSum(input, priority_map);
    try testing.expectEqual(sum, 70);
}

// Useful stdlib functions
const tokenize = std.mem.tokenize;
const split = std.mem.split;
const indexOf = std.mem.indexOfScalar;
const indexOfAny = std.mem.indexOfAny;
const indexOfStr = std.mem.indexOfPosLinear;
const lastIndexOf = std.mem.lastIndexOfScalar;
const lastIndexOfAny = std.mem.lastIndexOfAny;
const lastIndexOfStr = std.mem.lastIndexOfLinear;
const trim = std.mem.trim;
const sliceMin = std.mem.min;
const sliceMax = std.mem.max;

const parseInt = std.fmt.parseInt;
const parseFloat = std.fmt.parseFloat;

const min = std.math.min;
const min3 = std.math.min3;
const max = std.math.max;
const max3 = std.math.max3;

const print = std.debug.print;
const assert = std.debug.assert;

const sort = std.sort.sort;
const asc = std.sort.asc;
const desc = std.sort.desc;

// Generated from template/template.zig.
// Run `zig build generate` to update.
// Only unmodified days will be updated.
