const std = @import("std");
const testing = std.testing;
const Allocator = std.mem.Allocator;
const List = std.ArrayList;
const Map = std.AutoHashMap;
const StrMap = std.StringHashMap;
const BitSet = std.DynamicBitSet;

const util = @import("util.zig");
const gpa = util.gpa;

const data = @embedFile("data/day04.txt");

pub fn main() !void {
    var input: []const u8 = data;
    var sum = try fullyContainedSum(input);
    print("part1: {d}\n", .{sum});

    sum = try overlappedSum(input);
    print("part2: {d}\n", .{sum});
}

pub fn fullyContainedSum(input: []const u8) !usize {
    var sum: usize = 0;
    var it = tokenize(u8, input, "\n");
    while (it.next()) |pair| {
        if (try isPairFullyContained(pair)) {
            sum += 1;
        }
    }
    return sum;
}

pub fn overlappedSum(input: []const u8) !usize {
    var sum: usize = 0;
    var it = tokenize(u8, input, "\n");
    while (it.next()) |pair| {
        if (try isPairOverlapped(pair)) {
            sum += 1;
        }
    }
    return sum;
}

pub fn getElfPair(pair: []const u8) ![2]Assignment {
    var outer = tokenize(u8, pair, ",");
    var elf_pair: [2]Assignment = undefined;
    var i: usize = 0;
    var j: usize = 0;
    while (outer.next()) |e| {
        var inner = tokenize(u8, e, "-");
        while (inner.next()) |s| {
            if (j == 0) {
                elf_pair[i].start = try parseInt(usize, s, 0);
            }
            if (j == 1) {
                elf_pair[i].stop = try parseInt(usize, s, 0);
                j = 0;
                break;
            }
            j += 1;
        }

        if (i == 1) {
            break;
        }

        i += 1;
    }

    return elf_pair;
}

pub fn isPairFullyContained(pair: []const u8) !bool {
    var full_contained: bool = false;
    var elf_pair = try getElfPair(pair);

    if (elf_pair[0].start >= elf_pair[1].start and
        elf_pair[0].stop <= elf_pair[1].stop)
    {
        full_contained = true;
    }

    if (elf_pair[1].start >= elf_pair[0].start and
        elf_pair[1].stop <= elf_pair[0].stop)
    {
        full_contained = true;
    }

    return full_contained;
}

pub fn isPairOverlapped(pair: []const u8) !bool {
    var overlapped: bool = true;
    var elf_pair = try getElfPair(pair);

    var not_overlap = (elf_pair[0].stop < elf_pair[1].start or
        elf_pair[1].stop < elf_pair[0].start);

    if (not_overlap) {
        overlapped = false;
    }

    return overlapped;
}

const Assignment = struct {
    start: usize,
    stop: usize,
};

test "fully contained sum" {
    var input = (
        \\2-4,6-8
        \\2-3,4-5
        \\5-7,7-9
        \\2-8,3-7
        \\6-6,4-6
        \\2-6,4-8
    );

    var sum = try fullyContainedSum(input);
    try testing.expectEqual(sum, 2);
}

test "overlapped sum" {
    var input = (
        \\2-4,6-8
        \\2-3,4-5
        \\5-7,7-9
        \\2-8,3-7
        \\6-6,4-6
        \\2-6,4-8
    );

    var sum = try overlappedSum(input);
    try testing.expectEqual(sum, 4);
}

// Useful stdlib functions
const tokenize = std.mem.tokenize;
const split = std.mem.split;
const indexOf = std.mem.indexOfScalar;
const indexOfAny = std.mem.indexOfAny;
const indexOfStr = std.mem.indexOfPosLinear;
const lastIndexOf = std.mem.lastIndexOfScalar;
const lastIndexOfAny = std.mem.lastIndexOfAny;
const lastIndexOfStr = std.mem.lastIndexOfLinear;
const trim = std.mem.trim;
const sliceMin = std.mem.min;
const sliceMax = std.mem.max;

const parseInt = std.fmt.parseInt;
const parseFloat = std.fmt.parseFloat;

const min = std.math.min;
const min3 = std.math.min3;
const max = std.math.max;
const max3 = std.math.max3;

const print = std.debug.print;
const assert = std.debug.assert;

const sort = std.sort.sort;
const asc = std.sort.asc;
const desc = std.sort.desc;

// Generated from template/template.zig.
// Run `zig build generate` to update.
// Only unmodified days will be updated.
