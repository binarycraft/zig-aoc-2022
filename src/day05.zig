const std = @import("std");
const testing = std.testing;
const ascii = std.ascii;
const Allocator = std.mem.Allocator;
const List = std.ArrayList;
const Map = std.AutoHashMap;
const StrMap = std.StringHashMap;
const BitSet = std.DynamicBitSet;

const util = @import("util.zig");
const gpa = util.gpa;

const data = @embedFile("data/day05.txt");

pub fn main() !void {
    var input: []const u8 = data;
    var res = try parseInputPart1(input);
    print("part1: {s}\n", .{res});

    res = try parseInputPart2(input);
    print("part2: {s}\n", .{res});
}

pub fn parseInputPart1(input: []const u8) ![]const u8 {
    var it = split(u8, input, "\n\n");
    var it_crates = splitBackwards(u8, it.first(), "\n");

    var hdr = it_crates.first();
    var len = try parseInt(u8, &[_]u8{hdr[hdr.len - 2]}, 0);

    var crates = List(List(u8)).init(gpa);
    defer crates.deinit();
    defer for (crates.items) |i| i.deinit();

    var j: usize = 0;
    while (j < len) : (j += 1) {
        try crates.append(List(u8).init(gpa));
    }

    while (it_crates.next()) |row| {
        var idx: usize = 0;
        for (row) |r, i| {
            if (i >= 1 and (i - 1) % 4 == 0) {
                if (ascii.isUpper(r)) {
                    try crates.items[idx].append(r);
                }
                idx += 1;
            }
        }
    }

    var it_mv = tokenize(u8, it.rest(), "\n");
    while (it_mv.next()) |m| {
        var i: usize = 0;
        var inner = tokenize(u8, m, " ");
        var num: usize = undefined;
        var from: usize = undefined;
        var to: usize = undefined;

        while (inner.next()) |tok| {
            if (i == 1) {
                num = try parseInt(usize, tok, 0);
            }

            if (i == 3) {
                from = try parseInt(usize, tok, 0);
            }

            if (i == 5) {
                to = try parseInt(usize, tok, 0);
                i = 0;
            }

            i += 1;
        }

        var pop: usize = 0;
        while (pop < num) : (pop += 1) {
            var len_from = crates.items[from - 1].items.len;
            try crates.items[to - 1].append(
                crates.items[from - 1].items[len_from - 1],
            );
            _ = crates.items[from - 1].popOrNull();
        }
    }

    var res = try gpa.alloc(u8, crates.items.len);
    for (crates.items) |c, i| {
        res[i] = c.items[c.items.len - 1];
    }

    return res;
}

pub fn parseInputPart2(input: []const u8) ![]const u8 {
    var it = split(u8, input, "\n\n");
    var it_crates = splitBackwards(u8, it.first(), "\n");

    var hdr = it_crates.first();
    var len = try parseInt(u8, &[_]u8{hdr[hdr.len - 2]}, 0);

    var crates = List(List(u8)).init(gpa);
    defer crates.deinit();
    defer for (crates.items) |i| i.deinit();

    var j: usize = 0;
    while (j < len) : (j += 1) {
        try crates.append(List(u8).init(gpa));
    }

    while (it_crates.next()) |row| {
        var idx: usize = 0;
        for (row) |r, i| {
            if (i >= 1 and (i - 1) % 4 == 0) {
                if (ascii.isUpper(r)) {
                    try crates.items[idx].append(r);
                }
                idx += 1;
            }
        }
    }

    var it_mv = tokenize(u8, it.rest(), "\n");
    while (it_mv.next()) |m| {
        var i: usize = 0;
        var inner = tokenize(u8, m, " ");
        var num: usize = undefined;
        var from: usize = undefined;
        var to: usize = undefined;

        while (inner.next()) |tok| {
            if (i == 1) {
                num = try parseInt(usize, tok, 0);
            }

            if (i == 3) {
                from = try parseInt(usize, tok, 0);
            }

            if (i == 5) {
                to = try parseInt(usize, tok, 0);
                i = 0;
            }

            i += 1;
        }

        var len_from = crates.items[from - 1].items.len;
        if (num > len_from) {
            num = len_from;
        }

        try crates.items[to - 1].appendSlice(
            crates.items[from - 1].items[len_from - num ..],
        );

        var pop: usize = 0;
        while (pop < num) : (pop += 1) {
            _ = crates.items[from - 1].popOrNull();
        }
    }

    var res = try gpa.alloc(u8, crates.items.len);
    for (crates.items) |c, i| {
        res[i] = c.items[c.items.len - 1];
    }

    return res;
}

test "part 1" {
    var input = (
        \\    [D]    
        \\[N] [C]    
        \\[Z] [M] [P]
        \\ 1   2   3 
        \\
        \\move 1 from 2 to 1
        \\move 3 from 1 to 3
        \\move 2 from 2 to 1
        \\move 1 from 1 to 2
    );
    var res = try parseInputPart1(input);
    try testing.expectEqualSlices(u8, res, "CMZ");
}

test "part 2" {
    var input = (
        \\    [D]    
        \\[N] [C]    
        \\[Z] [M] [P]
        \\ 1   2   3 
        \\
        \\move 1 from 2 to 1
        \\move 3 from 1 to 3
        \\move 2 from 2 to 1
        \\move 1 from 1 to 2
    );
    var res = try parseInputPart2(input);
    try testing.expectEqualSlices(u8, res, "MCD");
}

// Useful stdlib functions
const tokenize = std.mem.tokenize;
const split = std.mem.split;
const window = std.mem.window;
const splitBackwards = std.mem.splitBackwards;
const indexOf = std.mem.indexOfScalar;
const indexOfAny = std.mem.indexOfAny;
const indexOfStr = std.mem.indexOfPosLinear;
const lastIndexOf = std.mem.lastIndexOfScalar;
const lastIndexOfAny = std.mem.lastIndexOfAny;
const lastIndexOfStr = std.mem.lastIndexOfLinear;
const trim = std.mem.trim;
const sliceMin = std.mem.min;
const sliceMax = std.mem.max;

const parseInt = std.fmt.parseInt;
const parseFloat = std.fmt.parseFloat;

const min = std.math.min;
const min3 = std.math.min3;
const max = std.math.max;
const max3 = std.math.max3;

const print = std.debug.print;
const assert = std.debug.assert;

const sort = std.sort.sort;
const asc = std.sort.asc;
const desc = std.sort.desc;

// Generated from template/template.zig.
// Run `zig build generate` to update.
// Only unmodified days will be updated.
