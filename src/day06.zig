const std = @import("std");
const testing = std.testing;
const Allocator = std.mem.Allocator;
const List = std.ArrayList;
const Map = std.AutoHashMap;
const StrMap = std.StringHashMap;
const BitSet = std.DynamicBitSet;

const util = @import("util.zig");
const gpa = util.gpa;

const data = @embedFile("data/day06.txt");

pub fn main() !void {
    var input: []const u8 = data;
    var res = startOfPacket(input);
    print("part1: {d}\n", .{res});

    res = startOfMessage(input);
    print("part2: {d}\n", .{res});
}

pub fn startOfPacket(input: []const u8) usize {
    var idx: usize = 0;
    win: for (input[0 .. input.len - 4]) |_, j| {
        var frame = input[j .. j + 4];
        outer: for (frame) |c_out, i| {
            for (frame[i + 1 ..]) |c_in| {
                if (c_out == c_in) {
                    break :outer;
                }

                if (frame.len - 1 == i + 1) {
                    idx = j;
                    break :win;
                }
            }
        }
    }
    return idx + 4;
}

pub fn startOfMessage(input: []const u8) usize {
    var idx: usize = 0;
    win: for (input[0 .. input.len - 14]) |_, j| {
        var frame = input[j .. j + 14];
        outer: for (frame) |c_out, i| {
            for (frame[i + 1 ..]) |c_in| {
                if (c_out == c_in) {
                    break :outer;
                }

                if (frame.len - 1 == i + 1) {
                    idx = j;
                    break :win;
                }
            }
        }
    }
    return idx + 14;
}

test "part 1" {
    var input1 = (
        \\bvwbjplbgvbhsrlpgdmjqwftvncz
    );
    var res = startOfPacket(input1);
    try testing.expectEqual(res, 5);

    var input2 = (
        \\nppdvjthqldpwncqszvftbrmjlhg
    );
    res = startOfPacket(input2);
    try testing.expectEqual(res, 6);

    var input3 = (
        \\nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg
    );
    res = startOfPacket(input3);
    try testing.expectEqual(res, 10);

    var input4 = (
        \\zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw
    );
    res = startOfPacket(input4);
    try testing.expectEqual(res, 11);
}

test "part 2" {
    var input1 = (
        \\bvwbjplbgvbhsrlpgdmjqwftvncz
    );
    var res = startOfMessage(input1);
    try testing.expectEqual(res, 23);

    var input2 = (
        \\nppdvjthqldpwncqszvftbrmjlhg
    );
    res = startOfMessage(input2);
    try testing.expectEqual(res, 23);

    var input3 = (
        \\nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg
    );
    res = startOfMessage(input3);
    try testing.expectEqual(res, 29);

    var input4 = (
        \\zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw
    );
    res = startOfMessage(input4);
    try testing.expectEqual(res, 26);
}

// Useful stdlib functions
const tokenize = std.mem.tokenize;
const split = std.mem.split;
const indexOf = std.mem.indexOfScalar;
const indexOfAny = std.mem.indexOfAny;
const indexOfStr = std.mem.indexOfPosLinear;
const lastIndexOf = std.mem.lastIndexOfScalar;
const lastIndexOfAny = std.mem.lastIndexOfAny;
const lastIndexOfStr = std.mem.lastIndexOfLinear;
const trim = std.mem.trim;
const sliceMin = std.mem.min;
const sliceMax = std.mem.max;

const parseInt = std.fmt.parseInt;
const parseFloat = std.fmt.parseFloat;

const min = std.math.min;
const min3 = std.math.min3;
const max = std.math.max;
const max3 = std.math.max3;

const print = std.debug.print;
const assert = std.debug.assert;

const sort = std.sort.sort;
const asc = std.sort.asc;
const desc = std.sort.desc;

// Generated from template/template.zig.
// Run `zig build generate` to update.
// Only unmodified days will be updated.
