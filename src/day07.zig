const std = @import("std");
const Allocator = std.mem.Allocator;
const List = std.ArrayList;
const Map = std.AutoHashMap;
const StrMap = std.StringHashMap;
const BitSet = std.DynamicBitSet;

const util = @import("util.zig");
const gpa = util.gpa;
const Str = util.Str;

const data = @embedFile("data/day07.txt");

const FileKind = enum {
    dir,
    file,
};

const File = struct {
    kind: FileKind,
    parent: ?*File,
    name: Str,
    children: ?List(*File),
    size: ?usize,
};

pub fn main() !void {
    var input: Str = data;
    var part1 = try part1Output(input);
    print("part1: {d}\n", .{part1});

    var part2 = try part2Output(input);
    print("part2: {d}\n", .{part2});
}

pub fn parseInput(input: Str) !List(File) {
    var i: usize = 0;
    var files = List(File).init(gpa);
    var children = List(List(File)).init(gpa);

    var it = tokenize(u8, input, "\n");
    while (it.next()) |line| {
        if (isChdir(line)) {
            if (!isRelative(line)) {
                var it_dir = split(u8, line, " ");
                assert(eql(u8, it_dir.first(), "$"));

                var name = it_dir.rest()[3..];
                try files.append(File{
                    .kind = FileKind.dir,
                    .parent = null,
                    .name = name,
                    .children = null,
                    .size = null,
                });
            }
        }

        if (isList(line)) {
            try children.append(List(File).init(gpa));
            while (it.next()) |file| {
                if (!isCmd(file)) {
                    var it_file = split(u8, file, " ");
                    var first = it_file.first();
                    var size: ?usize = undefined;
                    var kind: FileKind = undefined;

                    if (eql(u8, first, "dir")) {
                        kind = FileKind.dir;
                        size = null;
                    } else {
                        kind = FileKind.file;
                        size = try parseInt(usize, first, 0);
                    }
                    var name = it_file.rest();

                    try children.items[i].append(File{
                        .kind = kind,
                        .parent = null,
                        .name = name,
                        .children = null,
                        .size = size,
                    });
                }
                if (it.peek()) |p| {
                    if (isChdir(p)) {
                        break;
                    }
                }
            }
            i += 1;
        }
    }

    for (files.items) |*file, j| {
        file.children = List(*File).init(gpa);
        for (children.items[j].items) |*child| {
            child.parent = file;
            try file.children.?.append(child);
        }
    }

    for (files.items) |*file, j| {
        for (file.children.?.items) |child| {
            if (child.kind == FileKind.dir) {
                for (files.items[j + 1 ..]) |dir| {
                    if (eql(u8, dir.name, child.name)) {
                        child.children = dir.children;
                        break;
                    }
                }
            }
        }
    }

    return files;
}

pub fn part1Output(input: Str) !usize {
    var files = try parseInput(input);
    var sum: usize = 0;
    for (files.items) |file| {
        var dir_size = dirSize(file);
        if (dir_size <= 100000) {
            sum += dir_size;
        }
    }
    return sum;
}

pub fn part2Output(input: Str) !usize {
    var size: usize = undefined;
    var disk_size: usize = 70000000;
    var free_size: usize = undefined;
    var size_needed: usize = undefined;
    var files = try parseInput(input);

    var dir_size = dirSize(files.items[0]);
    assert(eql(u8, files.items[0].name, "/"));
    free_size = disk_size - dir_size;
    size_needed = 30000000 - free_size;

    for (files.items[1..]) |file| {
        dir_size = dirSize(file);
        if (dir_size >= size_needed) {
            if (size != undefined and
                dir_size < size)
            {
                size = dir_size;
            } else {
                size = dir_size;
            }
        }
    }
    return size;
}

pub fn dirSize(dir: File) usize {
    var size: usize = 0;
    for (dir.children.?.items) |child| {
        switch (child.kind) {
            .dir => {
                size += dirSize(child.*);
            },
            .file => {
                size += child.size.?;
            },
        }
    }
    return size;
}

pub fn isRelative(line: Str) bool {
    return endsWith(u8, line, "..");
}

pub fn isRoot(line: Str) bool {
    return endsWith(u8, line, "/");
}

pub fn isChdir(line: Str) bool {
    return startsWith(u8, line, "$ cd");
}

pub fn isList(line: Str) bool {
    return startsWith(u8, line, "$ ls");
}

pub fn isCmd(line: Str) bool {
    return startsWith(u8, line, "$ ");
}

test "part 1" {
    var input = (
        \\$ cd /
        \\$ ls
        \\dir a
        \\14848514 b.txt
        \\8504156 c.dat
        \\dir d
        \\$ cd a
        \\$ ls
        \\dir e
        \\29116 f
        \\2557 g
        \\62596 h.lst
        \\$ cd e
        \\$ ls
        \\584 i
        \\$ cd ..
        \\$ cd ..
        \\$ cd d
        \\$ ls
        \\4060174 j
        \\8033020 d.log
        \\5626152 d.ext
        \\7214296 k
    );
    var res = try part1Output(input);
    try testing.expectEqual(res, 95437);
}

test "part 2" {
    var input = (
        \\$ cd /
        \\$ ls
        \\dir a
        \\14848514 b.txt
        \\8504156 c.dat
        \\dir d
        \\$ cd a
        \\$ ls
        \\dir e
        \\29116 f
        \\2557 g
        \\62596 h.lst
        \\$ cd e
        \\$ ls
        \\584 i
        \\$ cd ..
        \\$ cd ..
        \\$ cd d
        \\$ ls
        \\4060174 j
        \\8033020 d.log
        \\5626152 d.ext
        \\7214296 k
    );
    var res = try part2Output(input);
    try testing.expectEqual(res, 24933642);
}

// Useful stdlib functions
const testing = std.testing;

const eql = std.mem.eql;
const tokenize = std.mem.tokenize;
const split = std.mem.split;
const startsWith = std.mem.startsWith;
const endsWith = std.mem.endsWith;
const indexOf = std.mem.indexOfScalar;
const indexOfAny = std.mem.indexOfAny;
const indexOfStr = std.mem.indexOfPosLinear;
const lastIndexOf = std.mem.lastIndexOfScalar;
const lastIndexOfAny = std.mem.lastIndexOfAny;
const lastIndexOfStr = std.mem.lastIndexOfLinear;
const trim = std.mem.trim;
const sliceMin = std.mem.min;
const sliceMax = std.mem.max;

const parseInt = std.fmt.parseInt;
const parseFloat = std.fmt.parseFloat;

const min = std.math.min;
const min3 = std.math.min3;
const max = std.math.max;
const max3 = std.math.max3;

const print = std.debug.print;
const assert = std.debug.assert;

const sort = std.sort.sort;
const asc = std.sort.asc;
const desc = std.sort.desc;

// Generated from template/template.zig.
// Run `zig build generate` to update.
// Only unmodified days will be updated.
