const std = @import("std");
const Allocator = std.mem.Allocator;
const List = std.ArrayList;
const Map = std.AutoHashMap;
const StrMap = std.StringHashMap;
const BitSet = std.DynamicBitSet;

const util = @import("util.zig");
const gpa = util.gpa;
const Str = util.Str;

const data = @embedFile("data/day08.txt");

pub fn main() !void {
    var input: Str = data;
    var part1 = try calculatePart1(input);
    print("part1: {d}\n", .{part1});

    var part2 = try calculatePart2(input);
    print("part2: {d}\n", .{part2});
}

pub fn inputToList(input: Str) !List(List(usize)) {
    var idx: usize = 0;
    var it_row = tokenize(u8, input, "\n");
    var map = List(List(usize)).init(gpa);
    while (it_row.next()) |row| {
        try map.append(List(usize).init(gpa));

        for (row) |r| {
            var num = try parseInt(usize, &[_]u8{r}, 0);
            try map.items[idx].append(num);
        }
        idx += 1;
    }
    return map;
}

pub fn calculatePart1(input: Str) !usize {
    var list = try inputToList(input);
    var r_len = list.items.len;
    var c_len = list.items[0].items.len;
    var count: usize = 2 * r_len + 2 * c_len - 4;
    for (list.items[1 .. r_len - 1]) |row, i| {
        for (row.items[1 .. c_len - 1]) |r, j| {
            var upper = List(usize).init(gpa);
            defer upper.deinit();
            for (list.items[0 .. i + 2]) |item| {
                try upper.append(item.items[j + 1]);
            }

            var lower = List(usize).init(gpa);
            defer lower.deinit();
            for (list.items[i + 1 ..]) |item| {
                try lower.append(item.items[j + 1]);
            }

            if (r == sliceMax(usize, upper.items) and
                countSlice(
                usize,
                upper.items,
                &[_]usize{r},
            ) == 1) {
                count += 1;
                continue;
            }

            if (r == sliceMax(usize, lower.items) and
                countSlice(
                usize,
                lower.items,
                &[_]usize{r},
            ) == 1) {
                count += 1;
                continue;
            }

            if (r == sliceMax(
                usize,
                list.items[i + 1].items[j + 1 ..],
            ) and countSlice(
                usize,
                list.items[i + 1].items[j + 1 ..],
                &[_]usize{r},
            ) == 1) {
                count += 1;
                continue;
            }

            if (r == sliceMax(
                usize,
                list.items[i + 1].items[0 .. j + 2],
            ) and countSlice(
                usize,
                list.items[i + 1].items[0 .. j + 2],
                &[_]usize{r},
            ) == 1) {
                count += 1;
                continue;
            }
        }
    }
    return count;
}

pub fn calculatePart2(input: Str) !usize {
    var score: usize = 0;
    var list = try inputToList(input);
    var r_len = list.items.len;
    var c_len = list.items[0].items.len;
    var scores = List(usize).init(gpa);
    for (list.items[1 .. r_len - 1]) |row, i| {
        for (row.items[1 .. c_len - 1]) |r, j| {
            _ = r;
            var upper = List(usize).init(gpa);
            defer upper.deinit();
            for (list.items[0 .. i + 2]) |item| {
                try upper.append(item.items[j + 1]);
            }
            var score_up = getScoreReverse(
                usize,
                upper.items,
            );

            var lower = List(usize).init(gpa);
            defer lower.deinit();
            for (list.items[i + 1 ..]) |item| {
                try lower.append(item.items[j + 1]);
            }
            var score_down = getScore(
                usize,
                lower.items,
            );
            var score_left = getScoreReverse(
                usize,
                list.items[i + 1].items[0 .. j + 2],
            );
            var score_right = getScore(
                usize,
                list.items[i + 1].items[j + 1 ..],
            );
            score = score_up * score_down *
                score_left * score_right;
            try scores.append(score);
        }
    }
    return sliceMax(usize, scores.items);
}

pub fn getScore(comptime T: type, slice: []const T) usize {
    var score = slice.len - 1;
    for (slice[1..]) |elem, i| {
        if (elem >= slice[0]) {
            score = i + 1;
            break;
        }
    }
    return score;
}

pub fn getScoreReverse(comptime T: type, slice: []const T) usize {
    var score = slice.len - 1;
    var idx: usize = 0;
    var it = reverseIterator(slice[0 .. slice.len - 1]);
    while (it.next()) |elem| {
        if (elem >= slice[slice.len - 1]) {
            score = idx + 1;
            break;
        }
        idx += 1;
    }
    return score;
}

test "part 1" {
    var input = (
        \\30373
        \\25512
        \\65332
        \\33549
        \\35390
    );

    var part1 = try calculatePart1(input);
    try testing.expectEqual(part1, 21);
}

test "part 2" {
    var input = (
        \\30373
        \\25512
        \\65332
        \\33549
        \\35390
    );

    var part2 = try calculatePart2(input);
    try testing.expectEqual(part2, 8);
}

// Useful stdlib functions
const testing = std.testing;

const tokenize = std.mem.tokenize;
const reverse = std.mem.reverse;
const split = std.mem.split;
const countSlice = std.mem.count;
const reverseIterator = std.mem.reverseIterator;
const indexOf = std.mem.indexOfScalar;
const indexOfAny = std.mem.indexOfAny;
const indexOfStr = std.mem.indexOfPosLinear;
const indexOfMax = std.mem.indexOfMax;
const lastIndexOf = std.mem.lastIndexOfScalar;
const lastIndexOfAny = std.mem.lastIndexOfAny;
const lastIndexOfStr = std.mem.lastIndexOfLinear;
const collapseRepeats = std.mem.collapseRepeats;
const trim = std.mem.trim;
const sliceMin = std.mem.min;
const sliceMax = std.mem.max;

const parseInt = std.fmt.parseInt;
const parseFloat = std.fmt.parseFloat;

const min = std.math.min;
const min3 = std.math.min3;
const max = std.math.max;
const max3 = std.math.max3;

const print = std.debug.print;
const assert = std.debug.assert;

const sort = std.sort.sort;
const asc = std.sort.asc;
const desc = std.sort.desc;

// Generated from template/template.zig.
// Run `zig build generate` to update.
// Only unmodified days will be updated.
