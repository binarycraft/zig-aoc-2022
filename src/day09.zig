const std = @import("std");
const Allocator = std.mem.Allocator;
const List = std.ArrayList;
const Map = std.AutoHashMap;
const StrMap = std.StringHashMap;
const BitSet = std.DynamicBitSet;

const util = @import("util.zig");
const gpa = util.gpa;
const Str = util.Str;

const data = @embedFile("data/day09.txt");

const Dir = enum {
    up,
    right,
    left,
    down,
};

const Rope = struct {
    head: Point,
    tail: Point,
    tail_list: List(Point),

    const Self = @This();
    pub fn init(x: isize, y: isize) !Self {
        var rope: Rope = Rope{
            .head = Point{
                .x = x,
                .y = y,
            },
            .tail = Point{
                .x = x,
                .y = y,
            },
            .tail_list = List(Point).init(gpa),
        };
        try rope.tail_list.append(rope.tail);

        return rope;
    }

    pub fn move(self: *Self, dir: Dir, num: usize, tail_only: bool) !void {
        switch (dir) {
            .up => {
                var i: usize = 0;
                while (i < num) : (i += 1) {
                    if (!tail_only) {
                        self.head.y -= 1;
                    }

                    if (try self.needToMoveStraight()) {
                        try self.appendTailList();
                    }

                    if (try self.needToMoveDiagonal()) {
                        try self.appendTailList();
                    }
                }
            },
            .right => {
                var i: usize = 0;
                while (i < num) : (i += 1) {
                    if (!tail_only) {
                        self.head.x += 1;
                    }

                    if (try self.needToMoveStraight()) {
                        try self.appendTailList();
                    }

                    if (try self.needToMoveDiagonal()) {
                        try self.appendTailList();
                    }
                }
            },
            .left => {
                var i: usize = 0;
                while (i < num) : (i += 1) {
                    if (!tail_only) {
                        self.head.x -= 1;
                    }

                    if (try self.needToMoveStraight()) {
                        try self.appendTailList();
                    }

                    if (try self.needToMoveDiagonal()) {
                        try self.appendTailList();
                    }
                }
            },
            .down => {
                var i: usize = 0;
                while (i < num) : (i += 1) {
                    if (!tail_only) {
                        self.head.y += 1;
                    }

                    if (try self.needToMoveStraight()) {
                        try self.appendTailList();
                    }

                    if (try self.needToMoveDiagonal()) {
                        try self.appendTailList();
                    }
                }
            },
        }
    }

    fn appendTailList(self: *Self) !void {
        for (self.tail_list.items) |tail| {
            if (std.meta.eql(tail, self.tail)) {
                return;
            }
        }
        try self.tail_list.append(self.tail);
    }

    fn needToMoveStraight(self: *Self) !bool {
        if (self.head.x == self.tail.x and
            try absInt(self.head.y - self.tail.y) == 2)
        {
            if ((self.head.y - self.tail.y) > 0) {
                self.tail.y += 1;
            } else {
                self.tail.y -= 1;
            }
            assert(try absInt(self.head.y - self.tail.y) == 1);
            return true;
        }

        if (self.head.y == self.tail.y and
            try absInt(self.head.x - self.tail.x) == 2)
        {
            if ((self.head.x - self.tail.x) > 0) {
                self.tail.x += 1;
            } else {
                self.tail.x -= 1;
            }
            assert(try absInt(self.head.x - self.tail.x) == 1);
            return true;
        }
        return false;
    }

    fn needToMoveDiagonal(self: *Self) !bool {
        if (try absInt(self.head.x - self.tail.x) == 1 and
            try absInt(self.head.y - self.tail.y) == 2)
        {
            self.tail.x = self.head.x;
            if ((self.head.y - self.tail.y) > 0) {
                self.tail.y += 1;
            } else {
                self.tail.y -= 1;
            }
            assert(try absInt(self.head.y - self.tail.y) == 1);
            return true;
        }

        if (try absInt(self.head.y - self.tail.y) == 1 and
            try absInt(self.head.x - self.tail.x) == 2)
        {
            self.tail.y = self.head.y;
            if ((self.head.x - self.tail.x) > 0) {
                self.tail.x += 1;
            } else {
                self.tail.x -= 1;
            }
            assert(try absInt(self.head.x - self.tail.x) == 1);
            return true;
        }

        if (try absInt(self.head.y - self.tail.y) == 2 and
            try absInt(self.head.x - self.tail.x) == 2)
        {
            if ((self.head.y - self.tail.y) > 0) {
                self.tail.y += 1;
            } else {
                self.tail.y -= 1;
            }
            assert(try absInt(self.head.y - self.tail.y) == 1);

            if ((self.head.x - self.tail.x) > 0) {
                self.tail.x += 1;
            } else {
                self.tail.x -= 1;
            }
            assert(try absInt(self.head.x - self.tail.x) == 1);
            return true;
        }
        return false;
    }
};

const Point = struct {
    x: isize,
    y: isize,
};

pub fn charToDirection(char: u8) Dir {
    var dir: Dir = undefined;
    switch (char) {
        'R' => {
            dir = Dir.right;
        },
        'U' => {
            dir = Dir.up;
        },
        'L' => {
            dir = Dir.left;
        },
        'D' => {
            dir = Dir.down;
        },
        else => {},
    }
    return dir;
}

pub fn calculatePart1(input: Str) !usize {
    var rope = try Rope.init(0, 4);
    var it = tokenize(u8, input, "\n");
    while (it.next()) |step| {
        var it_step = split(u8, step, " ");
        var dir = charToDirection(it_step.first()[0]);
        var num = try parseInt(usize, it_step.rest(), 0);
        try rope.move(dir, num, false);
    }

    return rope.tail_list.items.len;
}

pub fn calculatePart2(input: Str) !usize {
    var ropes: [9]Rope = undefined;
    for (ropes) |*rope| {
        rope.* = try Rope.init(11, 15);
    }

    var it = tokenize(u8, input, "\n");
    while (it.next()) |step| {
        var idx: usize = 0;
        var it_step = split(u8, step, " ");
        var dir = charToDirection(it_step.first()[0]);
        var num = try parseInt(usize, it_step.rest(), 0);
        while (idx < num) : (idx += 1) {
            try ropes[0].move(dir, 1, false);

            for (ropes[1..]) |*rope, i| {
                rope.*.head = ropes[i].tail;
                try rope.*.move(dir, 1, true);
            }
        }
    }

    return ropes[ropes.len - 1].tail_list.items.len;
}

pub fn main() !void {
    var input: Str = data;
    var part1 = try calculatePart1(input);
    print("part1: {d}\n", .{part1});

    var part2 = try calculatePart2(input);
    print("part2: {d}\n", .{part2});
}

test "part 1" {
    var input = (
        \\R 4
        \\U 4
        \\L 3
        \\D 1
        \\R 4
        \\D 1
        \\L 5
        \\R 2
    );
    var part1 = try calculatePart1(input);
    try testing.expectEqual(part1, 13);
}

test "part 2" {
    var input = (
        \\R 5
        \\U 8
        \\L 8
        \\D 3
        \\R 17
        \\D 10
        \\L 25
        \\U 20
    );
    var part2 = try calculatePart2(input);
    try testing.expectEqual(part2, 36);
}

// Useful stdlib functions
const testing = std.testing;

const tokenize = std.mem.tokenize;
const split = std.mem.split;
const indexOf = std.mem.indexOfScalar;
const indexOfAny = std.mem.indexOfAny;
const indexOfStr = std.mem.indexOfPosLinear;
const lastIndexOf = std.mem.lastIndexOfScalar;
const lastIndexOfAny = std.mem.lastIndexOfAny;
const lastIndexOfStr = std.mem.lastIndexOfLinear;
const trim = std.mem.trim;
const sliceMin = std.mem.min;
const sliceMax = std.mem.max;

const parseInt = std.fmt.parseInt;
const parseFloat = std.fmt.parseFloat;

const min = std.math.min;
const min3 = std.math.min3;
const max = std.math.max;
const max3 = std.math.max3;
const absInt = std.math.absInt;

const print = std.debug.print;
const assert = std.debug.assert;

const sort = std.sort.sort;
const asc = std.sort.asc;
const desc = std.sort.desc;

// Generated from template/template.zig.
// Run `zig build generate` to update.
// Only unmodified days will be updated.
