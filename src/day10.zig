const std = @import("std");
const Allocator = std.mem.Allocator;
const List = std.ArrayList;
const Map = std.AutoHashMap;
const StrMap = std.StringHashMap;
const BitSet = std.DynamicBitSet;

const util = @import("util.zig");
const gpa = util.gpa;
const Str = util.Str;

const data = @embedFile("data/day10.txt");

pub fn main() !void {
    var input: Str = data;
    var part1 = try calculatePart1(input);
    print("part1: {d}\n", .{part1});

    var part2 = try calculatePart2(input);
    defer gpa.free(part2);
    print("part2: \n{s}\n", .{part2});
}

pub fn getCycles(input: Str) ![]isize {
    var reg_x: isize = 1;
    var num_to_add: isize = 0;
    var cycles = List(isize).init(gpa);
    defer cycles.deinit();
    var it = tokenize(u8, input, "\n");

    while (it.next()) |cycle| {
        var it_cycle = tokenize(u8, cycle, " ");
        if (it_cycle.next()) |ins| {
            if (std.mem.eql(u8, ins, "noop")) {
                try cycles.append(reg_x);
            }

            if (std.mem.eql(u8, ins, "addx")) {
                if (it_cycle.next()) |num_str| {
                    num_to_add = try parseInt(
                        isize,
                        num_str,
                        0,
                    );
                }
                try cycles.append(reg_x);
                try cycles.append(reg_x);

                reg_x += num_to_add;
            }
        }
    }

    return try cycles.toOwnedSlice();
}

pub fn calculatePart1(input: Str) !isize {
    var signal_sum: isize = 0;
    var cycles = try getCycles(input);
    defer gpa.free(cycles);

    signal_sum = cycles[19] * 20 +
        cycles[59] * 60 +
        cycles[99] * 100 +
        cycles[139] * 140 +
        cycles[179] * 180 +
        cycles[219] * 220;
    return signal_sum;
}

pub fn calculatePart2(input: Str) ![]u8 {
    var sprite = [_]isize{ 0, 1, 2 };
    var cycles = try getCycles(input);
    defer gpa.free(cycles);

    var output = List(u8).init(gpa);
    outer: for (cycles) |cycle, i| {
        if (i > 0) {
            for (sprite) |*idx| {
                idx.* += cycle -
                    cycles[i - 1];
            }
        }

        for (sprite) |idx| {
            if (i == idx) {
                try output.append('#');
                try printNextLine(i, &output);
                continue :outer;
            }
            if (i == (idx + 40)) {
                try output.append('#');
                try printNextLine(i, &output);
                continue :outer;
            }
            if (i == (idx + 80)) {
                try output.append('#');
                try printNextLine(i, &output);
                continue :outer;
            }
            if (i == (idx + 120)) {
                try output.append('#');
                try printNextLine(i, &output);
                continue :outer;
            }
            if (i == (idx + 160)) {
                try output.append('#');
                try printNextLine(i, &output);
                continue :outer;
            }
            if (i == (idx + 200)) {
                try output.append('#');
                try printNextLine(i, &output);
                continue :outer;
            }
            if (i == (idx + 240)) {
                try output.append('#');
                try printNextLine(i, &output);
                continue :outer;
            }
        }
        try output.append('.');
        try printNextLine(i, &output);
    }
    return try output.toOwnedSlice();
}

pub fn printNextLine(i: usize, output: *List(u8)) !void {
    if (i == 39) {
        try output.*.append('\n');
    }
    if (i == 79) {
        try output.*.append('\n');
    }
    if (i == 119) {
        try output.*.append('\n');
    }
    if (i == 159) {
        try output.*.append('\n');
    }
    if (i == 199) {
        try output.*.append('\n');
    }
    if (i == 239) {
        try output.*.append('\n');
    }
}

test "part 1" {
    var input = (
        \\addx 15
        \\addx -11
        \\addx 6
        \\addx -3
        \\addx 5
        \\addx -1
        \\addx -8
        \\addx 13
        \\addx 4
        \\noop
        \\addx -1
        \\addx 5
        \\addx -1
        \\addx 5
        \\addx -1
        \\addx 5
        \\addx -1
        \\addx 5
        \\addx -1
        \\addx -35
        \\addx 1
        \\addx 24
        \\addx -19
        \\addx 1
        \\addx 16
        \\addx -11
        \\noop
        \\noop
        \\addx 21
        \\addx -15
        \\noop
        \\noop
        \\addx -3
        \\addx 9
        \\addx 1
        \\addx -3
        \\addx 8
        \\addx 1
        \\addx 5
        \\noop
        \\noop
        \\noop
        \\noop
        \\noop
        \\addx -36
        \\noop
        \\addx 1
        \\addx 7
        \\noop
        \\noop
        \\noop
        \\addx 2
        \\addx 6
        \\noop
        \\noop
        \\noop
        \\noop
        \\noop
        \\addx 1
        \\noop
        \\noop
        \\addx 7
        \\addx 1
        \\noop
        \\addx -13
        \\addx 13
        \\addx 7
        \\noop
        \\addx 1
        \\addx -33
        \\noop
        \\noop
        \\noop
        \\addx 2
        \\noop
        \\noop
        \\noop
        \\addx 8
        \\noop
        \\addx -1
        \\addx 2
        \\addx 1
        \\noop
        \\addx 17
        \\addx -9
        \\addx 1
        \\addx 1
        \\addx -3
        \\addx 11
        \\noop
        \\noop
        \\addx 1
        \\noop
        \\addx 1
        \\noop
        \\noop
        \\addx -13
        \\addx -19
        \\addx 1
        \\addx 3
        \\addx 26
        \\addx -30
        \\addx 12
        \\addx -1
        \\addx 3
        \\addx 1
        \\noop
        \\noop
        \\noop
        \\addx -9
        \\addx 18
        \\addx 1
        \\addx 2
        \\noop
        \\noop
        \\addx 9
        \\noop
        \\noop
        \\noop
        \\addx -1
        \\addx 2
        \\addx -37
        \\addx 1
        \\addx 3
        \\noop
        \\addx 15
        \\addx -21
        \\addx 22
        \\addx -6
        \\addx 1
        \\noop
        \\addx 2
        \\addx 1
        \\noop
        \\addx -10
        \\noop
        \\noop
        \\addx 20
        \\addx 1
        \\addx 2
        \\addx 2
        \\addx -6
        \\addx -11
        \\noop
        \\noop
        \\noop
    );

    var res = try calculatePart1(input);
    try testing.expectEqual(res, 13140);
}

test "part 1" {
    var input = (
        \\addx 15
        \\addx -11
        \\addx 6
        \\addx -3
        \\addx 5
        \\addx -1
        \\addx -8
        \\addx 13
        \\addx 4
        \\noop
        \\addx -1
        \\addx 5
        \\addx -1
        \\addx 5
        \\addx -1
        \\addx 5
        \\addx -1
        \\addx 5
        \\addx -1
        \\addx -35
        \\addx 1
        \\addx 24
        \\addx -19
        \\addx 1
        \\addx 16
        \\addx -11
        \\noop
        \\noop
        \\addx 21
        \\addx -15
        \\noop
        \\noop
        \\addx -3
        \\addx 9
        \\addx 1
        \\addx -3
        \\addx 8
        \\addx 1
        \\addx 5
        \\noop
        \\noop
        \\noop
        \\noop
        \\noop
        \\addx -36
        \\noop
        \\addx 1
        \\addx 7
        \\noop
        \\noop
        \\noop
        \\addx 2
        \\addx 6
        \\noop
        \\noop
        \\noop
        \\noop
        \\noop
        \\addx 1
        \\noop
        \\noop
        \\addx 7
        \\addx 1
        \\noop
        \\addx -13
        \\addx 13
        \\addx 7
        \\noop
        \\addx 1
        \\addx -33
        \\noop
        \\noop
        \\noop
        \\addx 2
        \\noop
        \\noop
        \\noop
        \\addx 8
        \\noop
        \\addx -1
        \\addx 2
        \\addx 1
        \\noop
        \\addx 17
        \\addx -9
        \\addx 1
        \\addx 1
        \\addx -3
        \\addx 11
        \\noop
        \\noop
        \\addx 1
        \\noop
        \\addx 1
        \\noop
        \\noop
        \\addx -13
        \\addx -19
        \\addx 1
        \\addx 3
        \\addx 26
        \\addx -30
        \\addx 12
        \\addx -1
        \\addx 3
        \\addx 1
        \\noop
        \\noop
        \\noop
        \\addx -9
        \\addx 18
        \\addx 1
        \\addx 2
        \\noop
        \\noop
        \\addx 9
        \\noop
        \\noop
        \\noop
        \\addx -1
        \\addx 2
        \\addx -37
        \\addx 1
        \\addx 3
        \\noop
        \\addx 15
        \\addx -21
        \\addx 22
        \\addx -6
        \\addx 1
        \\noop
        \\addx 2
        \\addx 1
        \\noop
        \\addx -10
        \\noop
        \\noop
        \\addx 20
        \\addx 1
        \\addx 2
        \\addx 2
        \\addx -6
        \\addx -11
        \\noop
        \\noop
        \\noop
    );

    var output = (
        \\##..##..##..##..##..##..##..##..##..##..
        \\###...###...###...###...###...###...###.
        \\####....####....####....####....####....
        \\#####.....#####.....#####.....#####.....
        \\######......######......######......####
        \\#######.......#######.......#######.....
    );

    var res = try calculatePart2(input);
    defer gpa.free(res);
    try testing.expectEqualSlices(
        u8,
        res[0..output.len],
        output,
    );
}

// Useful stdlib functions
const testing = std.testing;

const tokenize = std.mem.tokenize;
const split = std.mem.split;
const indexOf = std.mem.indexOfScalar;
const indexOfAny = std.mem.indexOfAny;
const indexOfStr = std.mem.indexOfPosLinear;
const lastIndexOf = std.mem.lastIndexOfScalar;
const lastIndexOfAny = std.mem.lastIndexOfAny;
const lastIndexOfStr = std.mem.lastIndexOfLinear;
const trim = std.mem.trim;
const sliceMin = std.mem.min;
const sliceMax = std.mem.max;

const parseInt = std.fmt.parseInt;
const parseFloat = std.fmt.parseFloat;

const min = std.math.min;
const min3 = std.math.min3;
const max = std.math.max;
const max3 = std.math.max3;

const print = std.debug.print;
const assert = std.debug.assert;

const sort = std.sort.sort;
const asc = std.sort.asc;
const desc = std.sort.desc;

// Generated from template/template.zig.
// Run `zig build generate` to update.
// Only unmodified days will be updated.
