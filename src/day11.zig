const std = @import("std");
const Allocator = std.mem.Allocator;
const List = std.ArrayList;
const Map = std.AutoHashMap;
const StrMap = std.StringHashMap;
const BitSet = std.DynamicBitSet;

const util = @import("util.zig");
const gpa = util.gpa;
const Str = util.Str;

const data = @embedFile("data/day11.txt");

const Monkey = struct {
    list: List(usize),
    count: usize = 0,
    op_str: Str,
    act_str: Str,
    mod: usize,

    const Self = @This();
    pub fn init(items: []usize, op: Str, act: Str, mod: usize) !Self {
        var list = List(usize).init(gpa);
        try list.appendSlice(items);

        return Monkey{
            .list = list,
            .op_str = op,
            .act_str = act,
            .mod = mod,
        };
    }

    pub fn doOp(self: *Self, item: usize) !usize {
        var op0: usize = undefined;
        var op1: usize = undefined;
        var it = tokenize(u8, self.op_str, " ");
        var operand0 = it.next().?;
        var operator = it.next().?;
        var operand1 = it.next().?;

        if (std.mem.eql(u8, operand0, "old")) {
            op0 = item;
        } else {
            op0 = try parseInt(usize, operand0, 0);
        }

        if (std.mem.eql(u8, operand1, "old")) {
            op1 = item;
        } else {
            op1 = try parseInt(usize, operand1, 0);
        }

        var res: usize = undefined;
        switch (operator[0]) {
            '+' => {
                res = try std.math.add(usize, op0, op1);
            },
            '*' => {
                res = try std.math.mul(usize, op0, op1);
            },
            else => {
                return error.UnexpectedError;
            },
        }

        return res;
    }

    pub fn doAct(self: *Self, item: usize) !usize {
        var div: usize = undefined;
        var throw: usize = 0;
        var divisible: bool = false;
        var a = tokenize(u8, self.act_str, "\n");
        while (a.next()) |line| {
            if (startsWith(u8, line, "  Test: divisible by ")) {
                var len = "  Test: divisible by ".*.len;
                div = try parseInt(usize, line[len..], 0);
            }

            if ((item % div) == 0) {
                divisible = true;
            }

            if (startsWith(u8, line, "    If true: throw ")) {
                if (divisible) {
                    var len = "    If true: throw to monkey ".*.len;
                    throw = try parseInt(usize, line[len..], 0);
                }
            }

            if (startsWith(u8, line, "    If false: throw")) {
                if (!divisible) {
                    var len = "    If false: throw to monkey ".*.len;
                    throw = try parseInt(usize, line[len..], 0);
                }
            }
        }

        return throw;
    }
};

const Monkeys = struct {
    items: []Monkey,
    mods: usize,

    const Self = @This();
    pub fn init(input: Str) !Monkeys {
        var mods: usize = 1;
        var monkeys = List(Monkey).init(gpa);
        var it = split(u8, input, "\n\n");
        while (it.next()) |monkey| {
            var mod: usize = undefined;
            var act_str: Str = &[_]u8{};
            var op_str: Str = &[_]u8{};
            var list = List(usize).init(gpa);
            defer list.deinit();
            var m = tokenize(u8, monkey, "\n");
            while (m.next()) |line| {
                if (startsWith(u8, line, "  Starting items:")) {
                    var len = "  Starting items:".*.len;
                    var it_line = tokenize(u8, line[len..], " ,");
                    while (it_line.next()) |num| {
                        try list.append(try parseInt(usize, num, 0));
                    }
                }

                if (startsWith(u8, line, "  Operation: new =")) {
                    var len = "  Operation: new =".*.len;
                    op_str = line[len..];
                }

                if (startsWith(u8, m.peek().?, "  Test: divisible")) {
                    var len = "  Test: divisible by ".*.len;
                    mod = try parseInt(usize, m.peek().?[len..], 0);
                    act_str = monkey[m.index..];
                    break;
                }
            }

            mods *= mod;
            var new = try Monkey.init(list.items, op_str, act_str, mod);
            try monkeys.append(new);
        }

        return Monkeys{
            .items = try monkeys.toOwnedSlice(),
            .mods = mods,
        };
    }

    pub fn deinit(self: *Self) void {
        gpa.free(self.items);
        self.mods = undefined;
    }

    pub fn monkeyBusiness(self: *Self, rounds: usize, div: bool) !usize {
        var round: usize = 0;
        while (round < rounds) : (round += 1) {
            for (self.items) |*monkey| {
                while (monkey.list.items.len > 0) {
                    var len = monkey.list.items.len;
                    var item = monkey.list.items[len - 1];

                    if (!div) {
                        item = item % self.mods;
                    }

                    var worry = try monkey.doOp(item);

                    if (div) {
                        worry /= 3;
                    }

                    var next = try monkey.doAct(worry);

                    try self.items[next].list.append(worry);
                    _ = monkey.*.list.pop();
                    monkey.count += 1;
                }
            }
        }

        var inspection = List(usize).init(gpa);
        defer inspection.deinit();
        for (self.items) |monkey| {
            try inspection.append(monkey.count);
        }

        sort(usize, inspection.items, {}, desc(usize));
        return inspection.items[0] * inspection.items[1];
    }
};

pub fn parsePart1(input: Str) !usize {
    var monkeys = try Monkeys.init(input);
    defer monkeys.deinit();
    return try monkeys.monkeyBusiness(20, true);
}

pub fn parsePart2(input: Str) !usize {
    var monkeys = try Monkeys.init(input);
    defer monkeys.deinit();
    return try monkeys.monkeyBusiness(10000, false);
}

pub fn main() !void {
    var input: Str = data;
    var part1 = try parsePart1(input);
    print("part1: {d}\n", .{part1});

    var part2 = try parsePart2(input);
    print("part2: {d}\n", .{part2});
}

test "part 1" {
    var input = (
        \\Monkey 0:
        \\  Starting items: 79, 98
        \\  Operation: new = old * 19
        \\  Test: divisible by 23
        \\    If true: throw to monkey 2
        \\    If false: throw to monkey 3
        \\
        \\Monkey 1:
        \\  Starting items: 54, 65, 75, 74
        \\  Operation: new = old + 6
        \\  Test: divisible by 19
        \\    If true: throw to monkey 2
        \\    If false: throw to monkey 0
        \\
        \\Monkey 2:
        \\  Starting items: 79, 60, 97
        \\  Operation: new = old * old
        \\  Test: divisible by 13
        \\    If true: throw to monkey 1
        \\    If false: throw to monkey 3
        \\
        \\Monkey 3:
        \\  Starting items: 74
        \\  Operation: new = old + 3
        \\  Test: divisible by 17
        \\    If true: throw to monkey 0
        \\    If false: throw to monkey 1
    );

    var res = try parsePart1(input);
    try testing.expectEqual(res, 10605);
}

test "part 2" {
    var input = (
        \\Monkey 0:
        \\  Starting items: 79, 98
        \\  Operation: new = old * 19
        \\  Test: divisible by 23
        \\    If true: throw to monkey 2
        \\    If false: throw to monkey 3
        \\
        \\Monkey 1:
        \\  Starting items: 54, 65, 75, 74
        \\  Operation: new = old + 6
        \\  Test: divisible by 19
        \\    If true: throw to monkey 2
        \\    If false: throw to monkey 0
        \\
        \\Monkey 2:
        \\  Starting items: 79, 60, 97
        \\  Operation: new = old * old
        \\  Test: divisible by 13
        \\    If true: throw to monkey 1
        \\    If false: throw to monkey 3
        \\
        \\Monkey 3:
        \\  Starting items: 74
        \\  Operation: new = old + 3
        \\  Test: divisible by 17
        \\    If true: throw to monkey 0
        \\    If false: throw to monkey 1
    );

    var res = try parsePart2(input);
    try testing.expectEqual(res, 2713310158);
}

// Useful stdlib functions
const testing = std.testing;

const reverse = std.mem.reverse;
const tokenize = std.mem.tokenize;
const split = std.mem.split;
const indexOf = std.mem.indexOfScalar;
const indexOfAny = std.mem.indexOfAny;
const indexOfStr = std.mem.indexOfPosLinear;
const lastIndexOf = std.mem.lastIndexOfScalar;
const lastIndexOfAny = std.mem.lastIndexOfAny;
const lastIndexOfStr = std.mem.lastIndexOfLinear;
const trim = std.mem.trim;
const sliceMin = std.mem.min;
const sliceMax = std.mem.max;
const startsWith = std.mem.startsWith;
const endsWith = std.mem.endsWith;

const parseInt = std.fmt.parseInt;
const parseFloat = std.fmt.parseFloat;

const min = std.math.min;
const min3 = std.math.min3;
const max = std.math.max;
const max3 = std.math.max3;

const print = std.debug.print;
const assert = std.debug.assert;

const sort = std.sort.sort;
const asc = std.sort.asc;
const desc = std.sort.desc;

// Generated from template/template.zig.
// Run `zig build generate` to update.
// Only unmodified days will be updated.
